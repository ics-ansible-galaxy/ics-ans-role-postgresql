# ics-ans-role-postgresql

Ansible role to install the PostgreSQL database from RPMS.

## Role Variables

```yaml
postgresql_version: 14
postgres_stat: "/var/lib/pgsql/{{ postgresql_version }}/data/PG_VERSION"
postgres_init: "/usr/pgsql-{{ postgresql_version }}/bin/initdb -D /var/lib/pgsql/{{ postgresql_version }}/data"
url: https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
postgresql_data:
  - user:
    password:
    dbname:
    attr:
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-postgresql
```

## License

BSD 2-clause
