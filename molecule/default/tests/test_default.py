import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_default(host):
    postgresql = host.service("postgresql-14.service")
    assert postgresql.is_running
    assert postgresql.is_enabled
